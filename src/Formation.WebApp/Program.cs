using System;
using System.Linq;
using System.Text;

using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Formation.WebApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}