using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Formation.Account.Data;
using Formation.MongoDb;

using Microsoft.AspNetCore.Mvc;

namespace Formation.WebApp.Controllers
{
    [Route("api/DemoData")]
    public class DemoDataController : Controller
    {
        private readonly MongoRepository<AccountSummaryDocument> _accountRepository;

        public DemoDataController(MongoRepository<AccountSummaryDocument> accountRepository)
        {
            this._accountRepository = accountRepository;
        }


        [HttpGet("all")]
        public async Task<IEnumerable<AccountSummaryDocument>> WeatherForecasts(int startDateIndex)
        {
            var document = new AccountSummaryDocument()
            {
                AccountId = Guid.NewGuid(),
                Total = 0,
            };

            await this._accountRepository.InsertOneAsync(document);

            var all = await this._accountRepository.FindAllAsync();
            return all;
        }
    }
}