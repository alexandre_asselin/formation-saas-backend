﻿using System;
using System.Linq;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Formation.EventStore
{
    /// <summary>
    /// Json converter that strips version and public token from type information in order to avoid deserialization errors when project version is incremented
    /// </summary>
    public class TypeConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is Type)
            {
                // here you decide how much information you really want to dump
                Type type = (Type)value;
                writer.WriteValue(type.FullName + ", " + type.Assembly.GetName().Name);
            }
            else
            {
                JToken t = JToken.FromObject(value);
                t.WriteTo(writer);
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = (string)reader.Value;

            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            // Remove version from type name and token from type
            var values = value.Split(',');

            if (values.Length < 2)
            {
                throw new Newtonsoft.Json.JsonSerializationException($"Error converting value {value} to type 'System.Type'");
            }

            var typeName = $"{values[0]}, {values[1]}";

            var result = Type.GetType(typeName, false);

            if (result == null)
            {
                throw new Newtonsoft.Json.JsonSerializationException($"Error converting value {value} to type 'System.Type'. The Type cannot be found");
            }

            return result;
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(System.Type).IsAssignableFrom(objectType);
        }
    }
}