﻿using System;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Formation.EventStore
{
    public class JsonEventSerializer
    {
        private readonly JsonSerializerSettings _serializerSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Auto
        };

        public JsonEventSerializer()
        {
            this._serializerSettings.Converters.Add(new TypeConverter());
        }

        public string ToString(object message)
        {
            return JsonConvert.SerializeObject(message, this._serializerSettings);
        }

        public byte[] ToByteArray(object @event)
        {
            var json = this.ToString(@event);

            return Encoding.UTF8.GetBytes(json);
        }

        public object FromString(Type type, string message)
        {
            return JsonConvert.DeserializeObject(message, type, this._serializerSettings);
        }

        public object FromByteArray(Type type, byte[] utf8EncodedJsonBytes)
        {
            var json = Encoding.UTF8.GetString(utf8EncodedJsonBytes);

            return this.FromString(type, json);
        }
    }
}