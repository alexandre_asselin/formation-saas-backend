﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Formation.EventStore
{
    public delegate Task HandleEvent(Guid eventId, DomainEvent @event, string streamId, long eventNumber, CancellationToken cancellationToken);
}