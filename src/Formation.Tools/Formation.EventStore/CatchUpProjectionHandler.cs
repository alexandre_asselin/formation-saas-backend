﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using EventStore.ClientAPI;
using EventStore.ClientAPI.Exceptions;

using Formation.Common.Configuration;

using Serilog;
using Serilog.Context;

namespace Formation.EventStore
{
    public class CatchUpProjectionHandler
    {
        private const int MaxLiveQueueSize = 1024;
        private readonly string _subscriptionName;
        private readonly string _streamName;
        private readonly Func<IEventStoreConnection> _connection;
        private readonly IPersistStreamCheckpoint _persistStreamCheckpoint;

        private readonly Dictionary<Type, HandleEvent> _handlers;

        private readonly Action _liveProcessingStartedHandler;
        private readonly JsonEventSerializer _eventSerializer;
        private EventStoreCatchUpSubscription _subscription;
        private long? _currentCheckpoint;
        private int _subscriptionCount;

        public CatchUpProjectionHandler(
            string subscriptionName,
            string streamName,
            Func<IEventStoreConnection> connection,
            IPersistStreamCheckpoint persistStreamCheckpoint,
            Dictionary<Type, HandleEvent> handlers,
            Action liveProcessingStartedHandler,
            JsonEventSerializer eventSerializer)
        {
            this._subscriptionName = subscriptionName;
            this._streamName = streamName;
            this._connection = connection;
            this._persistStreamCheckpoint = persistStreamCheckpoint;
            this._handlers = handlers;
            this._liveProcessingStartedHandler = liveProcessingStartedHandler;
            this._eventSerializer = eventSerializer;

            this.ProcessingState = ProcessingStatus.Catchup;
            this.SubscriptionState = SubscriptionStatus.NotStarted;
        }

        public string SubscriptionName => this._subscriptionName;

        public ProcessingStatus ProcessingState { get; private set; }

        public SubscriptionStatus SubscriptionState { get; private set; }

        public Exception FailureException { get; private set; }

        public long? CurrentCheckpoint => this._currentCheckpoint;

        public string LastEventStreamId { get; private set; }

        public string LastEventType { get; private set; }

        public long? LastEventOriginalNumber { get; private set; }

        public void SubscribeFrom(long? checkpoint)
        {
            this.SubscribeToStream(checkpoint);
        }

        public void Drop()
        {
            this._subscription.Stop();
        }

        public void RestartSubscription()
        {
            if (this.SubscriptionState == SubscriptionStatus.Running)
            {
                throw new InvalidOperationException("Cannot restart a subscription that is already running");
            }

            this._subscription.Stop();
            this._subscriptionCount++;
            this.SubscribeFrom(this._currentCheckpoint);
        }

        private CatchUpSubscriptionSettings GetSettings()
        {
            return new CatchUpSubscriptionSettings(
                maxLiveQueueSize: MaxLiveQueueSize,
                readBatchSize: 128,
                verboseLogging: ConfigurationService.GetSetting<bool>("EventStore:SubscriptionVerboseLogging"),
                resolveLinkTos: true,
                subscriptionName: $"{this._subscriptionName}-{this._subscriptionCount}");
        }

        private void SubscribeToStream(long? checkpoint)
        {
            this.SubscriptionState = SubscriptionStatus.Running;

            this._currentCheckpoint = checkpoint;
            this._subscription = this._connection()
                .SubscribeToStreamFrom(
                    this._streamName,
                    checkpoint,
                    this.GetSettings(),
                    this.OnEventAppearedAsync,
                    this.OnLiveProcessingStarted,
                    this.OnSubscriptionDropped);
        }

        private Task OnEventAppearedAsync(EventStoreCatchUpSubscription subscription, ResolvedEvent resolvedEvent)
        {
            using (LogContext.PushProperty("SubscriptionName", subscription.SubscriptionName))
            using (LogContext.PushProperty("EventStreamId", resolvedEvent.Event?.EventStreamId))
            using (LogContext.PushProperty("EventNumber", resolvedEvent.Event?.EventNumber))
            using (LogContext.PushProperty("EventType", resolvedEvent.Event?.EventType))
            using (LogContext.PushProperty("EventCheckpoint", resolvedEvent.OriginalEventNumber))
            using (LogContext.PushProperty("CurrentCheckpoint", this._currentCheckpoint))
            {
                return this.ProcessEventAsync(subscription, resolvedEvent);
            }
        }

        private async Task ProcessEventAsync(EventStoreCatchUpSubscription subscription, ResolvedEvent resolvedEvent)
        {
            if (this.SubscriptionState == SubscriptionStatus.Failed)
            {
                Log.Debug("Event appeared after the subscription failed");

                return;
            }

            if (resolvedEvent.Event == null)
            {
                Log.Debug("NullEventAppeared");

                return;
            }

            var eventCheckpoint = resolvedEvent.OriginalEventNumber;

            Log.Debug("SubscriptionEventAppeared");

            // EventStore catch up subscription offers at least once delivery and ordering of the events.
            // I.E if you receive an event it is garanteed that you have received all the previous event but
            // you migth receive the event twice. Thus you have to discard the duplicate event.
            // This might occur if you are debugging and set a breakpoint in the handler.
            // See: https://groups.google.com/forum/#!topic/event-store/AclJtshEopM
            if (eventCheckpoint <= this._currentCheckpoint)
            {
                Log.Warning("EventBeforeCheckpointAppeared");
                return;
            }

            try
            {
                var type = JsonEventTypeMapper.FromTypeName(resolvedEvent.Event.EventType);

                if (!this._handlers.TryGetValue(type, out var handler))
                {
                    Log.Verbose("No event handler was found for event type {eventType}", type);

                    return;
                }

                this.LastEventStreamId = resolvedEvent.Event.EventStreamId;
                this.LastEventType = resolvedEvent.Event.EventType;
                this.LastEventOriginalNumber = resolvedEvent.Event.EventNumber;

                var deserialized = this._eventSerializer.FromByteArray(type, resolvedEvent.Event.Data);
                var eventId = resolvedEvent.Event.EventId;

                if (deserialized is DomainEvent message)
                {
                    await handler.Invoke(
                        eventId,
                        message,
                        resolvedEvent.Event.EventStreamId,
                        resolvedEvent.Event.EventNumber,
                        CancellationToken.None);
                    Log.Debug("EventHandlerInvoked");
                }

                this._currentCheckpoint = resolvedEvent.OriginalEventNumber;

                // TODO: if we fail here for any reason the subscription is stopped but saved checkpoint is not valid
                // TODO: saving the checkpoint should be done in the document, i.e saving checkpoint should be atomic with the handler
                await this._persistStreamCheckpoint.SaveCheckpointAsync(this._subscriptionName, this._currentCheckpoint, resolvedEvent.OriginalEventNumber);

                Log.Debug("CheckpointSaved");
            }
            catch (Exception exception)
            {
                // Swallow exception since most of the time we want to handle failures in projections manually
                this.SubscriptionState = SubscriptionStatus.Failed;
                this.FailureException = exception;

                if (exception is ConnectionClosedException)
                {
                    Log.Error(exception, "SubcriptionHandlerError");
                }
                else
                {
                    Log.Fatal(exception, "SubcriptionHandlerError");
                }

                subscription.Stop();
            }
        }

        private void OnLiveProcessingStarted(EventStoreCatchUpSubscription subscription)
        {
            this.ProcessingState = ProcessingStatus.Live;
            this._liveProcessingStartedHandler?.Invoke();
        }

        private void OnSubscriptionDropped(EventStoreCatchUpSubscription subscription, SubscriptionDropReason reason, Exception exception)
        {
            using (LogContext.PushProperty("SubscriptionId", this._subscription.GetHashCode()))
            using (LogContext.PushProperty("SubscriptionName", this._subscriptionName))
            using (LogContext.PushProperty("StreamName", this._streamName))
            {
                var subscriptionNotFailed = this.SubscriptionState != SubscriptionStatus.Failed;

                var subscriptionFailedBecauseOfClosedConnection =
                    this.SubscriptionState == SubscriptionStatus.Failed && this.FailureException is ConnectionClosedException;

                var subscriptionDroppedBecauseOfClosedConnection = exception is ConnectionClosedException;

                if (subscriptionNotFailed ||
                    subscriptionFailedBecauseOfClosedConnection ||
                    subscriptionDroppedBecauseOfClosedConnection)
                {
                    this.SubscriptionState = SubscriptionStatus.Dropped;

                    Log.Warning(
                        exception,
                        "{logEventType} Reason: {subscriptionDropReason}. Resubscribing from checkpoint {checkpoint}.",
                        "SubscriptionDropped",
                        reason,
                        this._currentCheckpoint);

                    this.RestartSubscription();
                }
                else
                {
                    Log.Error(exception, "SubscriptionFailed");
                }
            }
        }
    }
}