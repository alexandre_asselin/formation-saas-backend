﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Formation.Common.Configuration;

using MongoDB.Bson;
using MongoDB.Driver;

namespace Formation.MongoDb
{
    public class MongoRepository<TDocument>
        where TDocument : MongoDocument
    {
        private readonly IMongoCollection<TDocument> _collection;

        public MongoRepository(CreateIndexesAsync createIndexesAsync)
            : this(
                ConfigurationService.GetSetting("MongoDb:ConnectionString"),
                createIndexesAsync)
        {
        }

        public MongoRepository(string connectionString, CreateIndexesAsync createIndexesAsync)
        : this(connectionString)
        {
            if (createIndexesAsync == null)
            {
                throw new ArgumentNullException(nameof(createIndexesAsync), "Index creation should at least include an index to enforce uniquity.");
            }

            Task.Run(async () => await createIndexesAsync(this._collection)).Wait();
        }

        public MongoRepository(string connectionString)
        {
            if (connectionString == null)
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            var mongoUrl = new MongoUrl(connectionString);
            var settings = MongoClientSettings.FromUrl(mongoUrl);

            var databaseName = mongoUrl.DatabaseName ?? "default";

            // For more information about the GUID representation, view https://studio3t.com/whats-new/best-practices-uuid-mongodb/
            settings.GuidRepresentation = GuidRepresentation.Standard;

            var client = new MongoClient(settings);
            var database = client.GetDatabase(databaseName);
            var collectionName = CollectionNameHelpers.GetCollectionNameForDocumentType<TDocument>();

            this._collection = database.GetCollection<TDocument>(collectionName);
        }

        public delegate Task CreateIndexesAsync(IMongoCollection<TDocument> collection);

        public Task InsertOneAsync(TDocument document)
        {
            return this._collection.InsertOneAsync(document);
        }

        public async Task<IReadOnlyList<TDocument>> FindAllAsync()
        {
            var result = new List<TDocument>();

            using (var cursor = await this._collection.FindAsync(_ => true))
            {
                while (await cursor.MoveNextAsync())
                {
                    result.AddRange(cursor.Current);
                }
            }

            return result;
        }

        public async Task<TDocument> FindOneAsync(FilterDefinition<TDocument> filter)
        {
            var matches = (await this.FindAsync(filter)).ToList();
            if (matches.Count > 1)
            {
                throw new MongoDuplicateDocumentException(nameof(this.FindOneAsync), typeof(TDocument).Name, matches);
            }

            return matches.FirstOrDefault();
        }

        public async Task<IEnumerable<TDocument>> FindAsync(FilterDefinition<TDocument> filter, FindOptions<TDocument, TDocument> options = null)
        {
            var result = new List<TDocument>();

            if (options == null)
            {
                using (var cursor = await this._collection.FindAsync(filter))
                {
                    while (await cursor.MoveNextAsync())
                    {
                        result.AddRange(cursor.Current);
                    }
                }
            }
            else
            {
                using (var cursor = await this._collection.FindAsync(filter, options))
                {
                    while (await cursor.MoveNextAsync())
                    {
                        result.AddRange(cursor.Current);
                    }
                }
            }

            return result;
        }

        public async Task<bool> AnyAsync(FilterDefinition<TDocument> filter)
        {
            return await this._collection.Find(filter).AnyAsync();
        }

        public async Task<bool> AllAsync(FilterDefinition<TDocument> filter, FilterDefinition<TDocument> predicate)
        {
            var allFilter = filter & Builders<TDocument>.Filter.Not(predicate);
            var hasAny = await this.AnyAsync(allFilter);

            return !hasAny;
        }

        public async Task<TDocument> UpsertOneAsync(FilterDefinition<TDocument> filter, UpdateDefinition<TDocument> updateDefinition)
        {
            var matches = (await this.FindAsync(filter)).ToList();
            if (matches.Count > 1)
            {
                throw new MongoDuplicateDocumentException(nameof(this.UpsertOneAsync), typeof(TDocument).Name, matches);
            }

            return await this._collection.FindOneAndUpdateAsync(filter, updateDefinition, new FindOneAndUpdateOptions<TDocument> { IsUpsert = true });
        }

        public async Task<TDocument> UpdateOneAsync(FilterDefinition<TDocument> filter, UpdateDefinition<TDocument> updateDefinition)
        {
            var matches = (await this.FindAsync(filter)).ToList();
            if (matches.Count > 1)
            {
                throw new MongoDuplicateDocumentException(nameof(this.UpdateOneAsync), typeof(TDocument).Name, matches);
            }

            return await this._collection.FindOneAndUpdateAsync(filter, updateDefinition);
        }

        public Task DeleteManyAsync(FilterDefinition<TDocument> filter)
        {
            return this._collection.DeleteManyAsync(filter);
        }

        public async Task DeleteOneAsync(FilterDefinition<TDocument> filter)
        {
            var matches = (await this.FindAsync(filter)).ToList();
            if (matches.Count > 1)
            {
                throw new MongoDuplicateDocumentException(nameof(this.DeleteOneAsync), typeof(TDocument).Name, matches);
            }

            await this._collection.DeleteOneAsync(filter);
        }

        // TODO: Tests
        public async Task<long> CountAsync(FilterDefinition<TDocument> filter)
        {
            var count = await this._collection.CountDocumentsAsync(filter);

            return count;
        }

        // TODO: Tests
        public async Task<List<T>> DistinctAsync<T>(FilterDefinition<TDocument> filter, string fieldName)
        {
            var results = new List<T>();

            using (var cursor = await this._collection.DistinctAsync<T>(fieldName, filter))
            {
                while (await cursor.MoveNextAsync())
                {
                    results.AddRange(cursor.Current);
                }
            }

            return results;
        }

        public Task UpsertManyAsync(IReadOnlyList<(FilterDefinition<TDocument> Filter, UpdateDefinition<TDocument> Update)> upserts)
        {
            if (!upserts.Any())
            {
                return Task.CompletedTask;
            }

            var upsertModels = upserts.Select(x => new UpdateOneModel<TDocument>(x.Filter, x.Update) { IsUpsert = true });

            return this._collection.BulkWriteAsync(upsertModels);
        }
    }
}