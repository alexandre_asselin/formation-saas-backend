﻿using System;
using System.Linq;
using System.Text;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Formation.MongoDb
{
    [BsonIgnoreExtraElements]
    public abstract class MongoDocument
    {
        [BsonId]
        public ObjectId Id { get; set; }
    }
}