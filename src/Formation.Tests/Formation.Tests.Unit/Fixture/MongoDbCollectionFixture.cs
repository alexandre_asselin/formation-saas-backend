﻿using System;
using System.Linq;
using System.Text;

using Xunit;

namespace Formation.Tests.Unit.Fixture
{
    [CollectionDefinition(XUnitCollections.MongoDb, DisableParallelization = true)]
    public sealed class MongoDbCollectionFixture : ICollectionFixture<MongoDbFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}