﻿using System;
using System.IO;
using System.Linq;
using System.Text;

using Formation.MongoDb;

using Mongo2Go;

using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace Formation.Tests.Unit.Fixture
{
    /// <summary>
    /// We don't use the Mongo2Go runner because for unknown reason the mongo server will become unresponsive and create
    /// timeout.
    /// The solution is to use the same command Mongo2Go used to start a mongo process.
    /// </summary>
    public sealed class MongoDbFixture : IDisposable
    {
        private const string MongoDbProcessName = "mongod";
        private const string Port = "27022";

        private static bool isAlreadyStarted;

        private readonly MongoDbRunner _runner;

        public MongoDbFixture()
        {
            if (isAlreadyStarted)
            {
                throw new InvalidOperationException("Mongo Fixture should not be instanciate twice. You probably used a ClassFixture instead of XUnitCollection somewhere.");
            }
            isAlreadyStarted = true;

            BsonSerializer.RegisterSerializer(typeof(DateTimeOffset), new DateTimeOffsetSerializer(BsonType.Document));

            this._runner = MongoDbRunner.Start();
        }

        private string BaseConnectionString => this._runner.ConnectionString;


        public MongoRepository<T> CreateMongoRepository<T>(MongoRepository<T>.CreateIndexesAsync createIndexesAsync)
            where T : MongoDocument
        {
            return new MongoRepository<T>(this.GetRandomConnectionString(), createIndexesAsync);
        }

        private string GetRandomConnectionString()
        {
            var randomName = Path.GetRandomFileName().Replace(".", string.Empty);
            return $"{this.BaseConnectionString}{randomName}";
        }

        public void Dispose()
        {
            this._runner.Dispose();
        }
    }
}