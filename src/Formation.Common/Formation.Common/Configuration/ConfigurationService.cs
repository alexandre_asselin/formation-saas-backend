﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;

using Microsoft.Extensions.Configuration;

using SGWA.Common.Configuration;

namespace Formation.Common.Configuration
{
    public static class ConfigurationService
    {
        private static IConfiguration configuration;

        public static string GetSetting(string key)
        {
            var setting = configuration[key];
            if (setting == null)
            {
                throw new SettingNotFoundException(key);
            }

            return setting;
        }

        public static T GetSetting<T>(string key)
        {
            var setting = GetSetting(key);

            return (T)Convert.ChangeType(setting, typeof(T), CultureInfo.InvariantCulture);
        }

        public static void SetConfiguration(IConfiguration configuration)
        {
            ConfigurationService.configuration = configuration;
        }
    }
}