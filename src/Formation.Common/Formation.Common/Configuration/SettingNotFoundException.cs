﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SGWA.Common.Configuration
{
    [Serializable]
    public class SettingNotFoundException : Exception
    {
        public SettingNotFoundException(string settingName)
            : base($"The setting with the key: {settingName} was not found. Add it to the appsettings files or key vault.")
        {
        }
    }
}
