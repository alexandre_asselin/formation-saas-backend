# README #

## Setup 

For the local dev environnement we use virtual machine to host a MongoDB and a EventStore. We use vagrant to provision those VS.

Steps you need to do before starting to have fun with the backend:

0. Clone the repository
1. Open powershell in admin and navigate into the "scripts" folder
2. Import the module containing multiple function useful for this formation with the command `Import-Module .\Formation-Module.psm1`  
- There might be a warning message, its normal.
3. If you don't have chocolatey installed it by running `Formation-Setup-Chocolatey`
4. Install required dependencies for the backend application (vagrant to provision 2 VM hosting MongoDB and EventStore): `Formation-Install-Dependencies`  
- There might be an error during the virtualbox installation, ignore it.
5. Install [nosqlbooster](https://nosqlbooster.com/downloads) or another tool of your choice to play with MongoDB

## Manage local Services (MongoDB and EventStore)

* Start Services (EventStore and MongoDB): `Formation-Start-Services`
* Reset services (clear data): `Formation-Reset-Services`
* Stop Services (EventStore and MongoDB): `Formation-Stop-Services`

### FAQ with services

* It ask a password for vagrant:

The password is: "vagrant"

* How can I connect to MongoDB

We use the default connection string (no user, no security!): "mongodb://localhost:27017"

* How can I play with EventStore

On your browser go to "http://127.0.0.1:2113" and use the credential: "admin" "changeit"


## FAQ ##

* Why powershell in admin?

Because we don't to loose time for side effects of not being admin (some dependencies might not be installed correctly)